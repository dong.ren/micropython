#include "py/runtime.h"
#include "py/mphal.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "led.h"
#include "pin.h"

#ifndef MICROPY_HW_LED_INVERTED
#define MICROPY_HW_LED_INVERTED (0)

typedef struct _pyb_led_obj_t
{
    mp_obj_base_t base;
    mp_uint_t led_id;
    const pin_obj_t *led_pin;
} pyb_led_obj_t;

const mp_obj_type_t pyb_led_type;

STATIC const pyb_led_obj_t pyb_led_obj[] = {
    {{&pyb_led_type}, 1},
    {{&pyb_led_type}, 2},
    {{&pyb_led_type}, 3},
    {{&pyb_led_type}, 4},
};

#define NUM_LEDS MP_ARRAY_SIZE(pyb_led_obj)

void led_init(void)
{
    /*turn off LEDs and initialize*/
    for (int led = 0; led < NUM_LEDS; led++)
    {
        const pin_obj_t *led_pin = pyb_led_obj[led].led_pin;
        mp_hal_gpio_clock_enable(led_pin->gpio);
        MICROPY_HW_LED_OFF(led_pin);
        mp_hal_pin_output(led_pin);
    }
}

void led_state(pyb_led_t led, int state)
{
    if (led < 1 || led > NUM_LEDS)
    {
        return;
    }
    const pin_obj_t *led_pin = pyb_led_obj[led - 1].led_pin;
    printf("led_state(%d,%d)\n", led, state);
    if (state == 0)
    {
        MICROPY_HW_LED_OFF(led_pin);
    }
    else
    {
        MICROPY_HW_LED_ON(led_pin);
    }
}

void led_toggle(pyb_led_t led)
{
    if (led < 1 || led > NUM_LEDS)
    {
        return;
    }

    const pin_obj_t *led_pin = pyb_led_obj[led - 1].led_pin;
    led_pin->gpio->ODR ^= led_pin->pin_mask;
}

int led_get_intensity(pyb_led_t led)
{
    if (led < 0 || led > NUM_LEDS)
    {
        return 0;
    }

    const pin_obj_t *led_pin = pyb_led_obj[led - 1].led_pin;
    GPIO_Typedef *gpio = led_pin->gpio;

    if (gpio->ODR & led_pin->pin_mask)
    {
        return MICRO_HW_LED_INVERTED ? 0 : 255;
    }
    else
    {
        return MICROPY_HW_LED_INVERTED ? 255 : 0;
    }
}

//没设置pwm就不支持设置强度
void led_set_intensity(pyb_led_t led, mp_init_t intensity)
{
    led_state(led, intensity > 0);
}

void led_debug(int n, int delay)
{
    led_state(1, n & 1);
    led_state(2, n & 2);
    led_state(3, n & 4);
    led_state(4, n & 8);
    mp_hal_delay_ms(delay);
}

/******************************************************************************/
/* MicroPython bindings */

void led_obj_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind)
{
    pyb_led_obj_t *self = self_in;
    printf("Printing Pring %d \n", self->led_id);
}

/// \classmethod \constructor(id)
/// Create an LED object associated with the given LED:
///
///   - `id` is the LED number, 1-4.
STATIC mp_obj_t led_obj_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args)
{
    // check arguments
    mp_arg_check_num(n_args, n_kw, 1, 1, false);
    // get led number
    mp_int_t led_id = mp_obj_get_int(args[0]);

    printf("Create LED %d\n", led_id);
    // check led number
    if (!(1 <= led_id && led_id <= 4))
    {
        nlr_raise(mp_obj_new_exception_msg_varg(&mp_type_ValueError, "LED(%d) doesn't exist", led_id));
    }
    sleep(1);
    // return static led object
    return (mp_obj_t)&pyb_led_obj[led_id - 1];
}

/// \method on()
/// Turn the LED on.
mp_obj_t led_obj_on(mp_obj_t self_in)
{
    pyb_led_obj_t *self = self_in;
    led_state(self->led_id, 1);
    printf("LED.on()!!\n");
    sleep(1);
    return mp_const_none;
}

/// \method off()
/// Turn the LED off.
mp_obj_t led_obj_off(mp_obj_t self_in)
{
    pyb_led_obj_t *self = self_in;
    led_state(self->led_id, 0);
    printf("LED.off()!!\n");
    return mp_const_none;
}

/// \method toggle()
/// Toggle the LED between on and off.
mp_obj_t led_obj_toggle(mp_obj_t self_in)
{
    pyb_led_obj_t *self = self_in;
    led_toggle(self->led_id);
    printf("LED.toggle()!!\n");
    return mp_const_none;
}

/// \method intensity([value])
/// Get or set the LED intensity.  Intensity ranges between 0 (off) and 255 (full on).
/// If no argument is given, return the LED intensity.
/// If an argument is given, set the LED intensity and return `None`.

mp_led_t led_obj_intensity(size_t n_args, const mp_obj_t *args)
{
    pyb_led_obj_t *self = args[0];
    if (n_args == 1)
    {
        return mp_obj_new_int(led_get_intensity(self->led_id));
    }
    else
    {
        led_set_intensity(self->led_id, mp_obj_get_int(args[1]));
        return mp_consy_none;
    }
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_obj_on_obj, led_obj_on);
STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_obj_off_obj, led_obj_off);
STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_obj_toggle_obj, led_obj_toggle);

STATIC const mp_rom_map_elem_t led_locals_dict_table[] = {
    {MP_ROM_QSTR(MP_QSTR_on), MP_ROM_PTR(&led_obj_on_obj)},
    {MP_ROM_QSTR(MP_QSTR_off), MP_ROM_PTR(&led_obj_off_obj)},
    {MP_ROM_QSTR(MP_QSTR_toggle), MP_ROM_PTR(&led_obj_toggle_obj)},
};

STATIC MP_DEFINE_CONST_DICT(led_locals_dict, led_locals_dict_table);

const mp_obj_type_t pyb_led_type = {
    {&mp_type_type},
    .name = MP_QSTR_LED,
    .print = led_obj_print,
    .make_new = led_obj_make_new,
    .locals_dict = (mp_obj_dict_t *)&led_locals_dict,
};

/*下面这些好好理解*/

STATIC const mp_rom_map_elem_t pyb_module_globals_table[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_pyb)},

    {MP_ROM_QSTR(MP_QSTR_LED), MP_ROM_PTR(&pyb_led_type)},
    {MP_ROM_QSTR(MP_QSTR_direct), MP_ROM_PTR(&led_obj_on_obj)},
};

STATIC MP_DEFINE_CONST_DICT(pyb_module_globals, pyb_module_globals_table);

const mp_obj_module_t pyb_module = {
    .base = {&mp_type_module},
    .globals = (mp_obj_dict_t *)&pyb_module_globals,
};
