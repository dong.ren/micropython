

#ifndef MIICROPY_INCLUDED_NUTTX_LED_H
#define MIICROPY_INCLUDED_NUTTX_LED_H

typedef enum {
    PYB_LED_R = 1,
    PYB_LED_G = 2,
    PYB_LED_B = 3,
    PYB_LED_O = 4,
} pyb_led_t;

void led_init(void);
void led_state(pyb_led_t led, int state);
void led_toggle(pyb_led_t led);
void led_debug(int value, int delay);

extern const mp_obj_type_t pyb_led_type;
extern

#endif
